package com.albrivas.takephotowithpermission

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import android.net.Uri.fromParts
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.support.v4.view.accessibility.AccessibilityEventCompat.setAction
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.provider.Settings
import android.widget.Toast
import org.jetbrains.anko.toast


class MainActivity : AppCompatActivity() {

    companion object {
        private const val PERMISSION_CAMERA = 100
        private const val ACTIVITY_RESULT = 200
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        actions()
    }

    private fun actions() {
        buttonCamera.setOnClickListener { getPictureFromCamera() }
    }

    // Take a photo asking permission runtime permissions
    private fun getPictureFromCamera(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), PERMISSION_CAMERA)
        }
        else {
            openCamera()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            PERMISSION_CAMERA -> {
                if(grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera()
                }
                else {
                    val uri = fromParts("package", packageName, null)
                    val intent = Intent().apply {
                        action = ACTION_APPLICATION_DETAILS_SETTINGS
                        data = uri
                    }
                    startActivityForResult(intent, ACTIVITY_RESULT)
                }

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode) {
            ACTIVITY_RESULT -> {
                if(resultCode == Activity.RESULT_OK) {
                    val extras = data!!.extras
                    val image = extras.get("data") as Bitmap
                    imageCamera.setImageBitmap(image)
                }
                else {
                    Toast.makeText(this, "No se ha podido hacer la foto", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    private fun openCamera() {
        startActivityForResult(Intent(MediaStore.ACTION_IMAGE_CAPTURE), ACTIVITY_RESULT)
    }
}
